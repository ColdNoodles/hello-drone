var expect = require('chai').expect;
var LogProcessor = require('../src/LogProcessor');

describe('LogProcessor', () => {
    describe('#floatMax()', ()=> {
        it('Should return the max of two floats', ()=> {
            var input1 = 11.11
            var test1 = 12.11;
            expect(LogProcessor.floatMax(input1, test1)).to.equal(12.11);

            var input1 = 11.11
            var test2 = 9.123
            expect(LogProcessor.floatMax(input1, test2)).to.equal(11.11);
        });
        it('Should accept the 2nd parameter as a string and return a float', () => {
            var input1 = 20.98;
            var test1 = '11.29';
            var test2 = '99.33121'
            expect(LogProcessor.floatMax(input1, test1)).to.equal(20.98);
            expect(LogProcessor.floatMax(input1, test2)).to.equal(99.33121);
        })
    });
    describe('#updateMinMaxObj()', ()=> {
        it('Should properly set the min and max fields', ()=> {
            const test1 = { min:0, max:0};
            LogProcessor.updateMinMaxObj(test1, 11)
            expect(test1).to.have.property('min').that.equals(0);
            expect(test1).to.have.property('max').that.equals(11);

            LogProcessor.updateMinMaxObj(test1, -11);
            expect(test1).to.have.property('min').that.equals(-11);
            expect(test1).to.have.property('max').that.equals(11);

            LogProcessor.updateMinMaxObj(test1, 0);
            expect(test1).to.have.property('min').that.equals(-11);
            expect(test1).to.have.property('max').that.equals(11);

            LogProcessor.updateMinMaxObj(test1, 23);
            expect(test1).to.have.property('min').that.equals(-11);
            expect(test1).to.have.property('max').that.equals(23);
        })
    });
    describe('#readATT()', ()=> {
        const input1 = 'ATT, 0.00, -0.26, -44.62, -20.02, 0.00, 244.71, 241.23';
        const line1 = input1.split(', ');
        const input2 = 'ATT, 0.00, 0.34, 0.00, -1.33, 0.00, 46.32, 46.32';
        const line2 = input2.split(', ');

        it('Should properly read the roll property from a line of data as a string', ()=> {
            expect(LogProcessor.readATT(line1)).to.have.property('roll').to.be.a('string').that.equals('-0.26');
            expect(LogProcessor.readATT(line2)).to.have.property('roll').to.be.a('string').that.equals('0.34');
        });
        it('Should properly read the pitch property from a line of data as a string', ()=> {
            expect(LogProcessor.readATT(line1)).to.have.property('pitch').to.be.a('string').that.equals('-20.02');
            expect(LogProcessor.readATT(line2)).to.have.property('pitch').to.be.a('string').that.equals('-1.33');
        });
        it('Should properly read the yaw property from a line of data as a string', ()=> {
            expect(LogProcessor.readATT(line1)).to.have.property('yaw').to.be.a('string').that.equals('244.71');
            expect(LogProcessor.readATT(line2)).to.have.property('yaw').to.be.a('string').that.equals('46.32');
        })
    });
    describe('#readCTUN()', () => {
        const input1 = 'CTUN, 623, 0.00, 652.77, 0.000000, 0, 29, 144, 652, 0'.split(', ');
        const input2 = 'CTUN, 0, 0.00, 161.09, 0.000000, 0, 0, -449, 1000, 0'.split(', ');
        it('Should properly read climbRate as a string', ()=> {
            expect(LogProcessor.readCTUN(input1)).to.have.property('climbRate').to.be.a('string').that.equals('144');
            expect(LogProcessor.readCTUN(input2)).to.have.property('climbRate').to.be.a('string').that.equals('-449');
        });
        it('Should properly read throttleInput as a string', ()=> {
            expect(LogProcessor.readCTUN(input1)).to.have.property('throttleOutput').to.be.a('string').that.equals('652');
            expect(LogProcessor.readCTUN(input2)).to.have.property('throttleOutput').to.be.a('string').that.equals('1000');
        });
        it('Should properly read throttleInput as a string', ()=> {
            expect(LogProcessor.readCTUN(input1)).to.have.property('throttleInput').to.be.a('string').that.equals('623');
            expect(LogProcessor.readCTUN(input2)).to.have.property('throttleInput').to.be.a('string').that.equals('0');
        });
    });
    describe('#readCURR()', () => {
        const input1 = 'CURR, 508, 49901, 1152, 1612, 4962, 71.042046'.split(', ');
        const input2 = 'CURR, 0, 2422500, 820, 1450, 5006, 1996.241800'.split(', ');
        it('Should properly read volt as a string', () => {
            expect(LogProcessor.readCURR(input1)).to.have.property('volt').to.be.a('string').that.equals('1152');
            expect(LogProcessor.readCURR(input2)).to.have.property('volt').to.be.a('string').that.equals('820');
        });
        it('Should properly read curr as a string', () => {
            expect(LogProcessor.readCURR(input1)).to.have.property('curr').to.be.a('string').that.equals('1612');
            expect(LogProcessor.readCURR(input2)).to.have.property('curr').to.be.a('string').that.equals('1450');
        });
        it('Should properly read currTotal as a string', () => {
            expect(LogProcessor.readCURR(input1)).to.have.property('currTotal').to.be.a('string').that.equals('71.042046');
            expect(LogProcessor.readCURR(input2)).to.have.property('currTotal').to.be.a('string').that.equals('1996.241800');
        });
    });
    describe('#readGPS()', () => {
        const input1 = 'GPS, 3, 530577600, 9, 2.67, 30.1542871, 118.8721889, 75.41, 758.44, 10.27, 39.98'.split(', ');
        const input2 = 'GPS, 3, 530865600, 9, 1.85, 30.1501976, 118.8716911, -10.61, 679.54, 7.02, 304.39'.split(', ');
        it('Should properly read lat as a string', () => {
            expect(LogProcessor.readGPS(input1)).to.have.property('lat').to.be.a('string').that.equals('30.1542871');
            expect(LogProcessor.readGPS(input2)).to.have.property('lat').to.be.a('string').that.equals('30.1501976');
        });
        it('Should properly read lng as a string', () => {
            expect(LogProcessor.readGPS(input1)).to.have.property('lng').to.be.a('string').that.equals('118.8721889');
            expect(LogProcessor.readGPS(input2)).to.have.property('lng').to.be.a('string').that.equals('118.8716911');
        });
        it('Should properly read alt as a string', () => {
            expect(LogProcessor.readGPS(input1)).to.have.property('alt').to.be.a('string').that.equals('75.41');
            expect(LogProcessor.readGPS(input2)).to.have.property('alt').to.be.a('string').that.equals('-10.61');
        });
        it('Should properly read relAlt as a string', () => {
            expect(LogProcessor.readGPS(input1)).to.have.property('relAlt').to.be.a('string').that.equals('758.44');
            expect(LogProcessor.readGPS(input2)).to.have.property('relAlt').to.be.a('string').that.equals('679.54');
        });
        it('Should properly read spd as a string', () => {
            expect(LogProcessor.readGPS(input1)).to.have.property('spd').to.be.a('string').that.equals('10.27');
            expect(LogProcessor.readGPS(input2)).to.have.property('spd').to.be.a('string').that.equals('7.02');
        });
        it('Should properly read gCrs as a string', () => {
            expect(LogProcessor.readGPS(input1)).to.have.property('gCrs').to.be.a('string').that.equals('39.98');
            expect(LogProcessor.readGPS(input2)).to.have.property('gCrs').to.be.a('string').that.equals('304.39');
        });

    });
})
