function disableForm() {
    $('input').prop('disabled', true);
    $('#loading').css('visibility','visible');
}

function resetForm() {
    $('#loading').css('visibility','hidden');
    $('form')[0].reset();
    $('input').prop('disabled', false);
}

function delayedAjaxResponse(ellapsedTime, minimumDelayTime, responseFunc) {
    if (ellapsedTime < minimumDelayTime) {
        setTimeout(()=>{
            responseFunc();
        }, minimumDelayTime - ellapsedTime);
    } else {
        responseFunc();
    }
}
$('form').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    disableForm();
    var ajaxStartTime = Date.now();
    $.ajax({
        url:'/upload',
        type:'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: data => {
            delayedAjaxResponse(Date.now() - ajaxStartTime, 1000, ()=>{
                resetForm();
                window.location.href = ('/report/' + data);
            });
        },
        error: error => {
            delayedAjaxResponse(Date.now() - ajaxStartTime, 1000, ()=>{
                resetForm();
                M.toast({html: "<span class='red-text'>Error: " + error.responseText +"</span>"});
            });
        }
    });
});
