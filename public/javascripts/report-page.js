
/**
 * Report Page
 * 
 * Assumes the following are in scope:
 * const obj - data from the server (const obj = !{JSON.stringify(data)} in pug template)
 */


/* Constants */
const CHART_POINTS_LIMIT = 40;

/* Helper Functions */
 /**
  * Given a duration timestamp, return it as a string in the form of minutes:seconds.milliseconds,
  * with seconds always having 2 digits and milliseconds having 3.
  *  
  * @param {String|Number} timestamp 
  */
function timeAsString(timestamp) {
    const duration = moment.duration(parseInt(timestamp));
    const minString = duration.minutes().toString();
    const secString = duration.seconds().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});
    const msString = duration.milliseconds().toLocaleString('en-US', {minimumIntegerDigits: 3, useGrouping:false});
    const timeString = minString + ":" + secString + '.' + msString;
    return timeString;
}

/**
 * Given a duration timestamp, return it as a string in the form of +minutes:seconds.milliseconds,
 * with seconds and milliseconds possibly varying in digit length.
 * @param {String|Number} timestamp 
 */
function relativeTimeAsString(timestamp) {
    const duration = moment.duration(parseInt(timestamp));
    const minString = duration.minutes();
    const secString = duration.seconds().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});
    const msString = duration.milliseconds();

    let output = "+"+minString+":"+secString; 
    if (msString > 0) {
        output += ("." + msString.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false}));
    } 
    return output;
}

/**
 * Converts a degree measurement from full 360 with 0/360 on top to positive/negative halves with 0 on top, 
 * -180 to the left, and +180 to the right
 * @param {Number} fullDegrees 
 */
function convertFullDegreesToHalfDegrees(fullDegrees) {
    if (fullDegrees > 180) {
        return fullDegrees - 360;
    }
    return fullDegrees;
}

/**
 * Given an array and a maximum amount of entries, produce
 * a copy of the array at a smaller "resolution" --
 * entries from the old array are uniformly selected into the new array,
 * retaining the array's general "shape" but reducing it's length).
 * Note: resulting array may be smaller than the allowed length.
 * @param {array} arr - original array
 * @param {number} maxLength - maximum length
 */
const decreaseArrayResolution = (arr, maxLength) => {
    return arr.filter((val, index, arr) => {
        let valsAllowed  = maxLength - 1;
        let includeEveryNth = Math.ceil((arr.length - 1) / valsAllowed);
        return (index == 0 || index == arr.length-1 || index % includeEveryNth == 0);
    });
};

/* Initialize Materialize Tabs */
var elem = $('.tabs')[0]
var tabs = M.Tabs.init(elem, {
    onShow: () => resizeAndFitMap(),
});