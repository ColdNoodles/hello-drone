/**
 * Battery Chart
 * 
 * Assumes the following are in scope:
 * - obj (drone log data, usually injected through template engine)
 * - Chart.js library (usually retrieved from CDN)
 * - report-page.js (contains helper functions and constants)
 */
const currArr = Object.entries(obj.log)
    .filter((entry) => {
        return ('CURR' in entry[1]);
    });

const currentPoints = decreaseArrayResolution(currArr, CHART_POINTS_LIMIT)
    .map(entry => {
        const relativeTime = entry[0] - obj.stats.time.start;
        const currentRemaining = obj.stats.battery.max - entry[1].CURR.currTotal;
        return {x: relativeTime, y: currentRemaining};
    });
var batteryCanvas = $('#battery-chart')[0].getContext('2d');
var batteryChart = new Chart(batteryCanvas, {
    type: 'scatter',
    data: {
        datasets: [{
            data: currentPoints,
            showLine: true,
            borderColor: '#009688',
            backgroundColor: '#80cbc480',
        }]
    },
    options: {
        responsive: true,
        scales: {
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Time since launch (min:sec.ms)',
                },
                ticks: {
                    callback: (value) => {
                        return relativeTimeAsString(value);
                    }
                }
            }],
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Capacity (mAh)',
                }
            }],
        },
        legend: {
            display: false,
        },
        tooltips: {
            displayColors: false,
            callbacks: {
                label: (tooltipItem, data) => {
                    let cap = tooltipItem.yLabel.toFixed(2);
                    let time = relativeTimeAsString(tooltipItem.xLabel);
                    return cap + ' mAh at ' + time;
                }
            }
        }
    }
});