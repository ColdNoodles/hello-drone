/**
 * Altitude Chart
 * 
 * Assumes the following are in scope:
 * - obj (drone log data, usually injected through template engine)
 * - Chart.js library (usually retrieved from CDN)
 * - report-page.js (contains helper functions and constants)
 */
const altData = {
    alt: [],
    relAlt: [],
};
decreaseArrayResolution(Object.entries(obj.log), CHART_POINTS_LIMIT)
    .reduce((acc, entry) => {
        const relativeTime = entry[0] - obj.stats.time.start;
        const alt = entry[1].GPS.alt;
        const relativeAlt = entry[1].GPS.relAlt;
        acc.alt.push({x: relativeTime, y: alt});
        acc.relAlt.push({x: relativeTime, y: relativeAlt});
        return acc;
    }, altData);

var altCanvas = $('#alt-chart')[0].getContext('2d');
var altChart = new Chart(altCanvas, {
    type: 'scatter',
    data: {
        datasets: [{
            label: 'GPS Altitude',
            data: altData.alt,
            showLine: true,
            fill: false,
            borderColor: '#ff9800',
            backgroundColor: '#ffcc8080',
        },
        {
            label: 'Barometer Altitude',
            data: altData.relAlt,
            showLine: true,
            fill: false,
            borderColor: '#009688',
            backgroundColor: '#80cbc480',
        }]
    },
    options: {
        responsive: true,
        scales: {
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Time since launch (min:sec.ms)',
                },
                ticks: {
                    callback: (value) => {
                        return relativeTimeAsString(value);
                    }
                }
            }],
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Altitude (m)',
                }
            }],
        },
        legend: {
            display: true,
        },
        tooltips: {
            displayColors: false,
            callbacks: {
                label: (tooltipItem, data) => {
                    let alt = tooltipItem.yLabel.toFixed(2);
                    let time = relativeTimeAsString(tooltipItem.xLabel);
                    return alt + ' m at ' + time;
                }
            }
        }
    }
});