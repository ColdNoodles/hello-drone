/**
 * Replay Viewer
 * 
 * Assumes the following are in scope:
 * - obj (drone log data, usually injected through template engine)
 * - report-page.js (contains helper functions and constants)
 */
let currentStep = 0;
let timerId;

const coords = [];
const timestamps = [];
const bearings = [];

const filteredRoute = Object.entries(obj.log)
.filter((elem, index, array) => {
    return (index == 0 || index === array.length-1 || index % 10 == 0);
});

filteredRoute.forEach(entry => {
    const timestamp = entry[0]
    const gps = entry[1].GPS;
    coords.push([gps.lng, gps.lat])
    timestamps.push(timestamp);
});

for (let i=0; i<filteredRoute.length; i++) {
    if (filteredRoute[i][1].ATT) {
        let yaw = parseFloat(filteredRoute[i][1].ATT.yaw);
        bearings[i] = convertFullDegreesToHalfDegrees(yaw);
    } else {
        bearings[i] = i>0 ? bearings[i-1] : 0;
    }
}
        
let timeDiff = parseInt(timestamps[timestamps.length-1]) - parseInt(timestamps[0]);
const totalTime = timeAsString(timeDiff);
var slider = $('#slider')[0]
noUiSlider.create(slider, {
    start: 0,
    connect: [true, false],
    step: 1,
    range: {
        'min': 0,
        'max': coords.length - 1,
    },
    format: {
        from: function(value) {
            return parseInt(value);
        },
        to: function(value) {
            return parseInt(value);
        }
    }
});


mapboxgl.accessToken = 'pk.eyJ1IjoiY29sZG5vb2RsZXMiLCJhIjoiY2pjcXR0OWR6MmpteDMzbng5eG85eXpzMSJ9.V5-ByefcObpAJgDcvdvU0w';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/coldnoodles/cjr03fkyj0c4g2srynms0ujl8'
});

map.addControl(new mapboxgl.ScaleControl({
    unit: 'imperial'
}));
var geojson = {
    "type": "FeatureCollection",
    "features": [{
        "type": "Feature",
        "geometry": {
            "type": "LineString",
            "properties": {},
            "coordinates": coords,
        }
    }]
};

var point = {
    type: 'Feature',
    geometry: {
        type: "Point",
        coordinates: coords[0]
    },
    properties: {

    }
}

map.on('load', () => {
    map.addLayer({
        "id": "LineString",
        "type": "line",
        "source": {
            "type": "geojson",
            "data": geojson
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#4db6ac",
            "line-width": 5
        }
    });

    map.addSource('point', {
        type: 'geojson',
        data: point
    });

    map.addLayer({
        id: 'point',
        source: 'point',
        type: 'symbol',
        layout: {
            'icon-image': 'drone-direction',
            "icon-rotate": ["get", "bearing"],
            'icon-allow-overlap': true,
        }
    })

    var bounds = coords.reduce((bounds, coord) => {
        return bounds.extend(coord);
    }, new mapboxgl.LngLatBounds(coords[0], coords[0]));

    

    map.fitBounds(bounds, {
        padding: 20
    });


    slider.noUiSlider.on('update', (values) => {
        point.geometry.coordinates = coords[values[0]];
        point.properties.bearing = bearings[values[0]];
        map.getSource('point').setData(point);

        let currentTime = parseInt(timestamps[values[0]]) - parseInt(timestamps[0])
        $('#time-label').text(timeAsString(currentTime) + " / " + totalTime);
    });
});

const playbackSpeeds = [400,200,100,50]
let selectedSpeed = 0;

function updateSpeedUI() {
    $('#speed-label').text((selectedSpeed+1) +'x Speed');
    if (selectedSpeed == 0) {
        $('#speed-down-btn').addClass('disabled');
    } else {
        $('#speed-down-btn').removeClass('disabled');
    }
    if (selectedSpeed == playbackSpeeds.length-1) {
        $('#speed-up-btn').addClass('disabled');
    } else {
        $('#speed-up-btn').removeClass('disabled');
    }
}

function resizeAndFitMap() {
    map.resize();        
    var bounds = coords.reduce((bounds, coord) => {
        return bounds.extend(coord);
    }, new mapboxgl.LngLatBounds(coords[0], coords[0]));

    map.fitBounds(bounds, {
        padding: 20
    });
}

function increaseSpeed() {
    selectedSpeed = Math.min(selectedSpeed + 1, playbackSpeeds.length - 1);
    updateSpeedUI();
}

function decreaseSpeed() {
    selectedSpeed = Math.max(0, selectedSpeed - 1);
    updateSpeedUI();
}

function replayTick() {
    let currentStep = slider.noUiSlider.get();
    if (currentStep >= coords.length-1) {
        pause();
    } else {
        slider.noUiSlider.set(++currentStep);
        timerId = setTimeout(replayTick, playbackSpeeds[selectedSpeed]);
    }
}

function pause() {
    clearTimeout(timerId);
    $('#play-pause-btn').off('click').click(play);
    $('#play-pause-btn i').text('play_arrow');
}

function play() {
    $('#play-pause-btn').off('click').click(pause);
    $('#play-pause-btn i').text('pause');
    setTimeout(replayTick, playbackSpeeds[selectedSpeed]);
}

$('#play-pause-btn').click(play);
$('#speed-up-btn').click(increaseSpeed);
$('#speed-down-btn').click(decreaseSpeed);
updateSpeedUI();