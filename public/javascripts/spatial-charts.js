/**
 * Spatial Charts
 * 
 * Assumes the following are in scope:
 * - jQuery
 * - Fabric.js library (usually retrieved from CDN)
 * - obj (drone log data, usually injected through template engine)
 */

 // Pitch Chart
 var pitchCanvas = new fabric.Canvas('pitch-canvas');
 var pitchDroneGroup = [];
 let pitchDrone;
 fabric.loadSVGFromURL('/images/drone_side.svg', (object, options) => {
     pitchDrone = new fabric.Group(pitchDroneGroup);
     pitchDrone.set({
         left: 50,
         top: 50,
         angle:0,
         scaleX: 2,
         scaleY: 2,
         originX: 'center',
         originY: 'center',
     });
     pitchCanvas.add(pitchDrone);
     resizePitchCanvas();
     animatePitchToMin();
 }, (item, object) => {
     object.set('id', item.getAttribute('id'));
     pitchDroneGroup.push(object);
 });
 
 function animatePitchToMin() {
     pitchDrone.animate('angle',obj.stats.pitchData.min, {
         duration: 2000,
         onChange: pitchCanvas.renderAll.bind(pitchCanvas),
         onComplete: animatePitchToMax,
         easing: fabric.util.ease.easeInOutCubic
     });
 }
 function animatePitchToMax() {
     pitchDrone.animate('angle',obj.stats.pitchData.max, {
         duration: 2000,
         onChange: pitchCanvas.renderAll.bind(pitchCanvas),
         onComplete: animatePitchToMin,
         easing: fabric.util.ease.easeInOutCubic
     });
 }
 function resizePitchCanvas() {
     const newWidth = $('#pitch-container').width();
     pitchCanvas.setWidth(newWidth);
     pitchDrone.center();
 }
 
 // Roll Chart
 var rollCanvas = new fabric.Canvas('roll-canvas');
 var rollDroneGroup = [];
 let rollDrone;
 fabric.loadSVGFromURL('/images/drone_front.svg', (object, options) => {
     rollDrone = new fabric.Group(rollDroneGroup);
     rollDrone.set({
         left: 50,
         top: 50,
         angle:0,
         scaleX: 2,
         scaleY: 2,
         originX: 'center',
         originY: 'center',
     });
     rollCanvas.add(rollDrone);
     resizeRollCanvas();
     animateRollToMin();
 }, (item, object) => {
     object.set('id', item.getAttribute('id'));
     rollDroneGroup.push(object);
 });
 
 function animateRollToMin() {
     rollDrone.animate('angle',obj.stats.rollData.min, {
         duration: 2000,
         onChange: rollCanvas.renderAll.bind(rollCanvas),
         onComplete: animateRollToMax,
         easing: fabric.util.ease.easeInOutCubic
     });
 }
 function animateRollToMax() {
     rollDrone.animate('angle',obj.stats.rollData.max, {
         duration: 2000,
         onChange: rollCanvas.renderAll.bind(rollCanvas),
         onComplete: animateRollToMin,
         easing: fabric.util.ease.easeInOutCubic
     });
 }
 function resizeRollCanvas() {
     const newWidth = $('#roll-container').width();
     rollCanvas.setWidth(newWidth);
     rollDrone.center();
 }
 
 $(window).resize(() => {
     resizePitchCanvas();
     resizeRollCanvas();
 });