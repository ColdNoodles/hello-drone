# Hello Drone

### Installation
1. Make sure Node.js and NPM are installed on your machine.
2. Make sure you have an internet connection.
3. Run `npm install`.

### Usage
To start the server, run `npm start`, then visit http://localhost:3000 in your 
web browser.

Once it's up and running, usage should be relatively straightforward. 
* Upload a drone log using the file picker, and it should take you to the report 
  page.
  * `2013-09-16 10-24 9.log` is the original sample log that was given to me.
* On the report page, you should see a report with interesting data, as well as 
  a separate 'replay' tab that will let you replay the path the drone took.
* Uploaded logs should be listed on the main page, so you can view previous 
  logs without having to re-upload them (if you don't see it, try refreshing the
  page first).

### Libraries, Frameworks, and Services Used
* Backend
  * Node.js
  * Express
    * starter project initialized by Express Application Generator (includes 
      Pug templating engine, SASS processor)
  * serve-favicon - Express middleware for serving favicons
    * node module
  * multer - Express middleware for handling multipart forms
    * node module
  * mapbox-static-paths - generate URLs for Mapbox's Static API
    * node module
  * Turf.js- to measure the distance of drone paths
    * node module
* Frontend
  * Materialize - Material Design-styled frontend framework
    * JS and SCSS
  * noUiSlider - JavaScript Range Slider
    * JS and SCSS
  * Moment.js - Used to format and dispaly timestamps
    * JS and node module
  * Chart.js - For dispalying graphs and charts
    * JS (linked to via CDN)
  * Fabric.js - For better drawing on canvas
    * JS (linked to via CDN)
  * Mapbox - display maps, generate static map images
    * JS and CSS (linked to via CDN)
  * JQuery - AJAX calls, UI interactions
    * JS (linked to via CDN)
  * Roboto - Font made by Google
    * CSS (linked to via CDN)
  * Material Icons - icon set made by Google
    * CSS (linked to via CDN)
  * Drone icon by Vectors Market
    * SVG (https://thenounproject.com/browse/?i=1198671)

That should be everything I used that I didn't personally create.
If it's not listed here, then either I originally made it, or it was included
as a part of the Express Application Generator and I didn't mess around with 
it, so I wasn't aware it was being used.

### Misc
If you want to run the unit tests I made, run `npm test`.

There isn't much of a difference between running the app in debug mode and 
running it regularly, but if you want to try debug mode anyways, run 
`npm debug`.
* If you're using MacOS or Linux, you might need to alter the debug entry in 
  package.json to be `DEBUG=hello-drone:* npm start` instead of 
  `set DEBUG=hello-drone:* & npm start` (which is Windows-specific).