var express = require('express');
var router = express.Router();
var multer = require('multer');
var path = require('path');
var mapboxPaths = require('mapbox-static-paths')('pk.eyJ1IjoiY29sZG5vb2RsZXMiLCJhIjoiY2pjcXR0OWR6MmpteDMzbng5eG85eXpzMSJ9.V5-ByefcObpAJgDcvdvU0w');
const LogProcessor = require('../src/LogProcessor');


// Multer / file upload setup
const uploadPath = path.join(process.cwd(), 'uploads');
const storage = multer.diskStorage({
	destination: uploadPath
});
const uploadLog = multer( {storage }).single('log');


// GET routes
router.get('/', async (req, res, next) => {
	try {
		const logs = await LogProcessor.getLogNames();
		res.render('index', { title: 'Hello Drone', logs });
	} catch(e) {
		next(e);
	}
});

router.get('/report/:logId', async (req, res, next) => {
	try {
		const filename = req.params.logId;
		const data = await LogProcessor.readLog(filename);
		let coordinates = Object.entries(data.log).map((entry) => {
			return [ parseFloat(entry[1].GPS.lat), parseFloat(entry[1].GPS.lng)];
		})
		const reducedPoints = coordinates.filter((value, index, array) => {
			return (index == 0 || index === array.length-1 || index % 10 == 0);

		});

		const staticMapUrl = mapboxPaths.setPath(
			reducedPoints,
			{
				strokecolor: '009688'
			}).getUrl({
				size: '500x250'
			});
		res.render('report', { data, logId: req.params.logId, staticMapUrl});
	} catch(e) {
		next(e);
	}
})


// POST routes
router.post('/upload', function(req, res) {
	uploadLog(req, res, function(err) {
		if (err || !req.file) {
		res.status(400).send('Selected file is invalid.');
		} else {
		res.status(201).send(req.file.filename);
		}
	});
});
	

module.exports = router;
