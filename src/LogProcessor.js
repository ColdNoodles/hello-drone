const fs = require('fs');
const {promisify} = require('util')

const logDirectoryPath = process.cwd() + '/uploads';
const readdir = promisify(fs.readdir);

const readline = require('readline');
const turfLength = require('@turf/length')
const turfHelpers = require('@turf/helpers');

const getLogNames = function() {
    return readdir(logDirectoryPath, {withFileTypes: true})
    .then(dirents => {
        const onlyFiles = dirents.filter(dirent => {
            return (dirent.isFile());
        });

        const returnArr = [];
        onlyFiles.forEach(fileDirent => {
            let filename = fileDirent.name;
            returnArr.push({
                name: filename,
                timestamp: fs.statSync(logDirectoryPath+"/"+filename).mtime
            });
        });
        return Promise.resolve(returnArr);
    }).catch(error => {
        return Promise.resolve([]);
    });
}

const updateMinMaxObj = (minMaxObj, value) => {
    let parsedValue = parseFloat(value);
    if (parsedValue < minMaxObj.min) {
        minMaxObj.min = parsedValue;
    }
    if (parsedValue > minMaxObj.max) {
        minMaxObj.max = parsedValue;
    }
}

const floatMax = (currentFloat, floatStr) => {
    let parsedFloat = parseFloat(floatStr);
    return Math.max(currentFloat, parsedFloat);
}

const readLog = (filename) => {
    return new Promise((resolve, reject) => {
        try{
            const filestream = fs.createReadStream(logDirectoryPath+"/"+filename);
            const rl = readline.createInterface({
                input: filestream,
                crlfDelay: Infinity,
            });

            const log = {};
            let currentTime;

            const stats = {
                time: {start: 0, end: 0},
                battery: {max:0, start:0, end:0},
                altitude: {max:0, relMax: 0},
                maxGroundSpeed: 0,
                pitchData: { min: 0, max: 0},
                yawData: { min: 0, max: 0},
                rollData: { min: 0, max: 0}
            }

            rl.on('line', (line) => {
                const entry = line.split(', ');
                switch (entry[0]) {
                    case 'PARM':
                        if (entry[1] == 'BATT_CAPACITY') {
                            stats.battery.max = parseFloat(entry[2]);
                        }
                        break;
                    case 'GPS':
                        const time = entry[2];
                        if (stats.time.start === 0) {
                            stats.time.start = time;
                        }
                        stats.time.end = time;
                        if (currentTime !== time) {
                            currentTime = time;
                            log[currentTime] = {};
                        }
                        let gps = readGPS(entry);
                        log[currentTime].GPS = gps;
                        stats.maxGroundSpeed = floatMax(stats.maxGroundSpeed, gps.spd);
                        stats.altitude.max = floatMax(stats.altitude.max, gps.alt);
                        stats.altitude.relMax = floatMax(stats.altitude.relMax, gps.relAlt);
                        break;
                    case 'ATT':
                        let att = readATT(entry)
                        log[currentTime].ATT = att;
                        updateMinMaxObj(stats.pitchData, att.pitch);
                        updateMinMaxObj(stats.yawData, att.yaw);
                        updateMinMaxObj(stats.rollData, att.roll);
                        break;
                    case 'CTUNE':
                        log[currentTime].CTUN = readCTUN(entry);
                        break;
                    case 'CURR':
                        let curr = readCURR(entry);
                        log[currentTime].CURR = curr;
                        if (stats.battery.start <= 0) {
                            stats.battery.start = curr.currTotal;
                        }
                        stats.battery.end = floatMax(stats.battery.end, curr.currTotal);
                        break;
                }
            }).on('close', () => {
                const line = turfHelpers.lineString(getCoordinateArrayFromLog(log));
                const distance = turfLength.default(line);
                stats.distanceKm = distance;
                resolve({
                    stats,
                    log,
                });
            })
        }
        catch (error) {
            reject(error);
        }
    });
}

/**
 * Generates an array of coordinate arrays based on log data.
 * @param {Object} log - log object. Can be obtained from LogProcessor.readLog() data object (data.log)
 * @param {Boolean} useLatLngOrder - if true, will output coordinates in [lat, lng]. Defaults to false, i.e. [lng, lat].
 */
const getCoordinateArrayFromLog = (log, useLatLngOrder = false) => {
    return Object.entries(log).map((entry) => {
        if (useLatLngOrder) {
            return [ parseFloat(entry[1].GPS.lat), parseFloat(entry[1].GPS.lng)];
        } else {
            return [ parseFloat(entry[1].GPS.lng), parseFloat(entry[1].GPS.lat) ];
        }
        
    })
}

const readCTUN = dataLine => {
    return {
        climbRate: dataLine[7],
        throttleOutput: dataLine[8],
        throttleInput: dataLine[1],
    }
}

const readATT = dataLine => {
    return {
        roll: dataLine[2],
        pitch: dataLine[4],
        yaw: dataLine[6],
    }
}

const readCURR = dataLine => {
    return {
        volt: dataLine[3],
        curr: dataLine[4],
        currTotal: dataLine[6],
    }
}

const readGPS = dataLine => {
    return {
        lat: dataLine[5],
        lng: dataLine[6],
        alt: dataLine[7],
        relAlt: dataLine[8],
        spd: dataLine[9],
        gCrs: dataLine[10],
    };
}


module.exports = {
    getLogNames,
    readLog,
    updateMinMaxObj,
    floatMax,
    getCoordinateArrayFromLog,
    readCTUN,
    readATT,
    readCURR,
    readGPS,
}